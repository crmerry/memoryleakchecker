#include <Windows.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#define ERROR_MSG_NOALLOC L"NOALLOC"
#define ERROR_MSG_ARRAY L"ARRAY"
#define ERROR_MSG_LEAK L"LEAK"

bool UTF8toUTF16(const char* text, wchar_t* buf, int buf_len)
{
	int trans_len = MultiByteToWideChar(CP_UTF8, 0, text, (int)strlen(text), buf, buf_len);

	if (trans_len < buf_len)
	{
		buf[trans_len] = L'\0';
	}

	return true;
}

enum class VARIABLE_TYPE
{
	VARIABLE = 0,
	ARRAY,
};

/* ***************************************
할당 정보 단위.
**************************************** */
typedef struct _AllocationSlot
{
	void*			_ptr = nullptr;
	VARIABLE_TYPE	_type = VARIABLE_TYPE::VARIABLE;
	unsigned int	_size = 0;
	unsigned int	_line = 0;
	wchar_t*		_file_name = nullptr;
	_AllocationSlot* _next = nullptr;
}AllocationSlot;

/* ***************************************
할당 정보를 리스트로 관리.
**************************************** */
class AllocationList
{
public:
	void Initialize()
	{
		_head = (AllocationSlot*)malloc(sizeof(AllocationSlot));

		_head->_ptr = nullptr;
		_head->_next = nullptr;
		_tail = _head;
	}

	void Insert(void* ptr, VARIABLE_TYPE type, unsigned int size, unsigned int line, wchar_t* file_name)
	{
		AllocationSlot* slot = (AllocationSlot*)malloc(sizeof(AllocationSlot));

		slot->_ptr = ptr;
		slot->_type = type;
		slot->_size = size;
		slot->_line = line;
		slot->_file_name = file_name;
		slot->_next = nullptr;

		_tail->_next = slot;
		_tail = _tail->_next;
		_tail->_next = nullptr;
	}

	bool Search(AllocationSlot** prev, AllocationSlot** next, void* memory)
	{
		*next = _head;

		while (1)
		{
			if (*next == nullptr)
			{
				return false;
			}

			if ((*next)->_ptr == memory)
			{
				return true;
			}

			*prev = *next;
			*next = (*next)->_next;
		}
	}
	
private:
	AllocationSlot* _head;
	AllocationSlot* _tail;

	//*******************************************************
	// 멤버 변수의 변경은 오로지 멤버 함수를 통해서만
	// 아래의 friend는 GetXXX와 같이 값만 가져오는 부분에서만 제한적으로 사용
	//*******************************************************
	friend class AllocationHandler;
};

/* ***************************************
로그 정보 단위
**************************************** */
typedef struct _LogInfoSlot
{
	wchar_t			_info[128];
	_LogInfoSlot*	_next;
}LogInfoSlot;

/* ***************************************
로그를 리스트 형태로 저장
**************************************** */
class LogList
{
public:
	void Initialize()
	{
		_head = (LogInfoSlot*)malloc(sizeof(LogInfoSlot));

		_head->_info[0] = '\0';
		_head->_next = nullptr;
		_tail = _head;
	}

	void Insert(wchar_t* msg)
	{
		LogInfoSlot* log = (LogInfoSlot*)malloc(sizeof(LogInfoSlot));

		log->_info[0] = '\0';
		log->_next = nullptr;

		wcscpy_s(log->_info, 128, msg);
		_tail->_next = log;
		_tail = _tail->_next;
	}

private:
	LogInfoSlot* _head;
	LogInfoSlot* _tail;

	//*******************************************************
	// 정보를 변경하는 일은 멤버함수로만 가능하게하고
	// 값을 얻기 위한 목적으로 friend를 둔다.
	//*******************************************************
	friend class AllocationHandler;
};

/* ***************************************
new가 호출되면 전역객체에 의하여 할당된 정보가 쌓이고
delete가 호출되면 전역객체에 의하여 해제 및 오류 정보가 쌓인다.
**************************************** */
class AllocationHandler
{
public:
	AllocationHandler()
	{
		InitializeCriticalSection(&_cs);
		//*******************************************************
		// 멤버 변수를 초기화 한다.
		//*******************************************************
		{
			struct tm t;
			time_t current;

			time(&current);
			localtime_s(&t, &current);
			
			swprintf_s(_file_name, 64, L"Alloc_%d%d%d_%d%d%d.txt", t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);
		}

		{
			_list = (AllocationList*)malloc(sizeof(AllocationList));
			_list->Initialize();
		}

		{
			_log_manager = (LogList*)malloc(sizeof(LogList));
			_log_manager->Initialize();
		}
	}

	//*******************************************************
	// 파괴자가 호출 될 때, LogList의 인스턴스에 쌓아둔 로그와
	// 아직 해제되지 않은 메모리에 대한 로그를 출력한다.
	//*******************************************************
	~AllocationHandler()
	{
		DeleteCriticalSection(&_cs);
		if (_total_alloc_count == 0)
		{
			return;
		}

		PrintLog();
	}

	void PrintLog()
	{
		_wfopen_s(&_log_file, _file_name, L"a");

		if (!_log_file)
		{
			return;
		}

		//*********************************************
		// ARRAY, NOALLOC
		//*********************************************

		LogInfoSlot* controller = _log_manager->_head->_next;

		while (1)
		{
			if (!controller)
			{
				break;
			}

			fwprintf(_log_file, L"%s\n", controller->_info);

			controller = controller->_next;
		}


		//*********************************************
		// LEAK
		//*********************************************
		AllocationSlot* slot = _list->_head;

		while (slot != nullptr)
		{
			if (slot->_ptr)
			{
				fwprintf(_log_file, L"%-10s [%p] [%6u] %s : %d\n", ERROR_MSG_LEAK, slot->_ptr, slot->_size, slot->_file_name, slot->_line);
			}

			slot = slot->_next;
		}

		//*********************************************
		// Total 정보.
		//*********************************************
		fwprintf(_log_file, L"----------------------------------------------------------\n");
		fwprintf(_log_file, L"Total Allocation Size : %u byte \n", _total_alloc_size);
		fwprintf(_log_file, L"Total Allocation Count : %u  \n", _total_alloc_count);
		
		fclose(_log_file);
	}

	//*********************************************
	// 파괴자가 있는 객체를 new를 하거나 delete를 할 때는 
	// delete 뒷단에서 +4, -4 계산을 알아서 해준다.
	// 그러므로, 해당 계산을 신경쓰지 않는다.
	//*********************************************
	void* MemoryAllocate(size_t size, wchar_t* file_name, int line, VARIABLE_TYPE type)
	{
		void* ptr = malloc(size);

		_list->Insert(ptr, type, (unsigned int)size, line, file_name);

		_total_alloc_count += 1;
		_total_alloc_size += (unsigned int)size;

		return ptr;
	}

	//*******************************************************
	// elete 를 오버로딩하여 메모리 삭제시 오류 확인.
	// 실제 할당된적이 있는 포인터인지, 배열형태는 맞는지)
	// 문제가 없었다면 할당내역 삭제
	// 오류가 났다면 해당 메모리 할당 정보에 오류코드 저장
	//*******************************************************
	void MemoryRelease(void* p, VARIABLE_TYPE type)
	{
		AllocationSlot* target = nullptr;
		AllocationSlot* prev = nullptr;
		wchar_t info[128];
		info[0] = '\0';

		if (_list->Search(&prev, &target, p))
		{
			if (target->_type == type)
			{
				if (target == _list->_tail)
				{
					_list->_tail = prev;
				}

				prev->_next = target->_next;
				free(target);
			}
			else
			{
				//*********************************************
				// 파괴자가 없는 클래스나 기본타입 변수들의 변수, 배열 미스매치 
				//*********************************************
				swprintf_s(info, 128, L"%-10s [%p] [%6u] %s : %d", ERROR_MSG_ARRAY, p, target->_size, target->_file_name, target->_line);
				_log_manager->Insert(info);
			}

			return;
		}

		if (_list->Search(&prev, &target, (void*)((char*)p + 8)) || _list->Search(&prev, &target, (void*)((char*)p - 8)))
		{
			//*********************************************
			// 파괴자가 있는 클래스의 변수, 배열 미스매치
			//*********************************************
			swprintf_s(info, 128, L"%-10s [%p] [%6u] %s : %d", ERROR_MSG_ARRAY, p, target->_size, target->_file_name, target->_line);
			_log_manager->Insert(info);

			return;
		}

		//*********************************************
		// NOALLOC
		//*********************************************
		swprintf_s(info, 128, L"%-10s [%p]", ERROR_MSG_NOALLOC, p);
		_log_manager->Insert(info);

		return;
	}

	void Lock()
	{
		EnterCriticalSection(&_cs);
	}

	void Unlock()
	{
		LeaveCriticalSection(&_cs);
	}

private:
	AllocationList*	_list;
	FILE*			_log_file;
	wchar_t			_file_name[64];
	LogList*		_log_manager;
	unsigned int	_total_alloc_count = 0;
	unsigned int	_total_alloc_size = 0;
	CRITICAL_SECTION _cs;
};

/* ***************************************
핸들러 전역 변수
**************************************** */
AllocationHandler g_memory_handler;

void* operator new (size_t size, char* File, int Line)
{
	wchar_t file_name[128];

	UTF8toUTF16(File, file_name, sizeof file_name / sizeof (wchar_t));

	g_memory_handler.Lock();

	void* result = g_memory_handler.MemoryAllocate(size, file_name, Line, VARIABLE_TYPE::VARIABLE);

	g_memory_handler.Unlock();

	return result;
}

void* operator new[](size_t size, char* File, int Line)
{
	wchar_t file_name[128];

	UTF8toUTF16(File, file_name, sizeof file_name / sizeof(wchar_t));

	g_memory_handler.Lock();

	void* result = g_memory_handler.MemoryAllocate(size, file_name, Line, VARIABLE_TYPE::ARRAY);

	g_memory_handler.Unlock();

	return result;
}

/* ***************************************
다음 delete 는 실제로 쓰진 않지만 문법상 컴파일 오류를 막기 위해 만듬
속은 비워둠.
그런데, vs2015에서는 없어도 컴파일 ok, 그러나 일단 넣어둠.
**************************************** */
void operator delete (void * p, char *File, int Line)
{
}

void operator delete[](void * p, char *File, int Line)
{
}

/* ***************************************
실제로 사용할 delete
파괴자가 있는 클래스에서 new를 이용하여 동적할당을 하면
실제 동적할당한 크기보다 4byte만큼을 더 쓰게 된다.
왜냐하면, 할당한 얼마만큼 할당했는지를 표기하기 때문.
때문에, delete로 오버로딩을 할 때 이부분을 신경써줘야 된다고 생각했으나
delete 호출시에 알아서 -4만큼 해서 돌려준다.
즉, 신경써줄 필요가 없다.(짝을 맞게 호출한다면)
**************************************** */
void operator delete (void* p)
{
	g_memory_handler.Lock();

	g_memory_handler.MemoryRelease(p, VARIABLE_TYPE::VARIABLE);

	g_memory_handler.Unlock();
}

void operator delete[](void * p)
{
	g_memory_handler.Lock();

	g_memory_handler.MemoryRelease(p, VARIABLE_TYPE::ARRAY);

	g_memory_handler.Unlock();
}
