// MemoryLeakChecker2.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include <stdio.h>
#include "MemoryLeakChecker.h"

class AAA
{
public:
	static int count;
	AAA();
	~AAA();
	char c[100];
};

AAA::AAA()
{
}

AAA::~AAA()
{
	printf("FUCK %d \n", count);
	count++;
}
int AAA::count = 0;

#if _WIN64
#define NUM 10
#else
#define NUM 20
#endif
int main()
{
	////1 .
	////************************************************
	//// delete가 호출되면 오버로딩을 했고 내 함수에서 어떤 처리를 안했더라도
	//// 변수값이 바뀌어 버린다... 
	//// 예를 들어
	//// int* pi = new int [100];
	//// delete pi;
	//// dlelete[] pi;
	//// 를 한다면, 첫 번째 delete해서 ARRAY 로그가 남고
	//// 두 번째 delete에 의하여 제대로 할당이 해제 되기를 기대하는데
	//// 실제로는 두 번째 delete에서 NOALLOC이 뜨면서
	//// 마지막에 또 LEAK이 뜬다.
	//// 이 부분은 어쩔 수 없나?
	////************************************************

	//int* pi = new int[100]; // leak
	//char* pc = new char[200]; // leak
	//int* pi2 = new int; // leak
	//char* pc2 = new char; // leak

	//AAA* pa = new AAA[100];
	//AAA* pa2 = new AAA; // LEAK
	//pa2->c[0] = 'a';
	//pa2->c[1] = '\0';

	//delete pi; // Array
	//delete[] pi; // NOALLOC

	//delete pc; // ARRAY
	//delete[] pc; // NOALLOC

	//delete[] pi2; //Array
	//delete pi2; // Noalloc


	//delete[] pa;
	////__int64* pa3 = ((__int64*)pa2-1);
	////*pa3 = 1;
	//delete pa2;
	//printf("%s", pa2->c);
	//int* pp1 = new int[400];
	//int* pp2 = new int;
	//int* pp3 = new int;
	//int* pp4 = new int[300];

	wprintf(L"%d \n", NUM);
	return 0;
}