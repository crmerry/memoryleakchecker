#ifndef  __MEMORY_LEAK_CHECKER__
#define __MEMORY_LEAK_CHECKER__

/* ***************************************
[MemoryLeakChecker]
수정 : 2018/03/16
TODO : char -> wchar_t 로 자료형 변환 

변수 new, 배열 new를 할 때마다 기록.
변수 delete, 배열 delete를 할 때마다 기록된 내용과 비교.
적절하게 호출되었는지를 확인.
잘못된 결과(할당하지 않은 것을 delete, 배열 & 변수 미스 매치, 할당 안됨)에 대하여 출력.

전역 변수 AllocationHandler g_memory_handler; 를 사용하며,
프로그램 종료시에 소멸자 호출에 의하여 결과를 텍스트 파일로 출력.
디버깅시, delete에서 is_direct 변수를 조작하여 바로 로그를 찍을 수도 있음.
혹은 바로 바로 g_memory_handler.PrintLog 호출.
**************************************** */
bool UTF8toUTF16(const char* text, wchar_t* buf, int buf_len);

void* operator new (size_t size, char* File, int Line);
void* operator new[](size_t size, char* File, int Line);

/* ***************************************
다음 delete 는 실제로 쓰진 않지만 문법상 컴파일 오류를 막기 위해 만듬
속은 비워둠.
그런데, vs2015에서는 없어도 컴파일 ok, 그러나 일단 넣어둠.
**************************************** */
void operator delete (void* p, char* File, int Line);
void operator delete[](void* p, char* File, int Line);

/* ***************************************
실제로 사용할 delete.
**************************************** */
void operator delete (void* p);
void operator delete[](void * p);

#define new new(__FILE__, __LINE__)
#endif // ! 


